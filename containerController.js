({
    
    doInit : function (cmp, event) {
        var action = cmp.get("c.getTemplateNames");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                cmp.set("v.templates", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleClick : function (cmp, event) {
        var value = event.detail.menuItem.get("v.value");
        var action = cmp.get("c.getTemplateById");
        action.setParams({"id":value});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                var updateTemplate = $A.get("e.c:flyerTemplateUpdate");
                var fullTemplate = response.getReturnValue();
                updateTemplate.setParams({"template": fullTemplate});
                updateTemplate.fire();
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
        console.log(value);
    },
    
  
    
    
 })