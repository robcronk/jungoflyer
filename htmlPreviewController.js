({
   
    showTemplate : function (cmp, event) {
        var template = event.getParam("template");
        var html = template.html__c;
        template = $("<html/>").html(html).text();
        var re = new RegExp(String.fromCharCode(160), "g");
        var test = template.replace(re, "");
        console.log(test);
        cmp.set("v.template", test);
    },
    
	jsLoaded: function(component, event, helper) {
		console.log("jquery loaded");
	},
      pdfClick : function (cmp, event) {
        var pdf = cmp.find("toPDF").get("v.value");
        console.log(pdf);
    },
    printClick : function (cmp, event) {
    
   
	},
         
})